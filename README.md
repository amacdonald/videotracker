# Video Tracker

## Description

Video Tracker is a project made by amacdonald and mdaneymonhar for the second semester of L1 in computer science. It is an application where you can load video and get a csv file of a manually tracked object on the video.

## Installation

This app work on any operating system but you must have python 3 and modules PIL, tkinter and cv2 installed on it.

## Usage

### Navigating through the menu bar

To navigate through the menu bar you simply have to click on the menu you want, it may open another menu bar.

### Opening files

You can open files with the menu "Open Video"

* :warning: It must be a **.mp4** file

### Reading the file

To read file you can either use the menu "Read video" or the "PLAY/PAUSE" button. You can't pause with "Read video".

### Mooving frame by frame

You can read the video forward and backward frame by frame with "<" and ">" buttons.

### Mooving to beginning / end

Use << button to got to the 1st frame of the video and >> button to go to the last frame

### Timeline

You can navigate into the video with the timeline at the bottom of the window

### Closing the file

You can close the file with "Quit Video" menu.

### Export Points

You can export your Points with the "Save Points" or "Save as" button. It will ask a directory. You can either create a new one or choose an already created one.

* :warning: Must be a **CSV** file
* :warning: Already created file will be **overwritten**

### Show a graph

You can show a graph with the menu View > Graphs > y(x). Datas are from a choosen .csv file with a 2 column with only int format.

* :warning: Must be a **CSV** file

### Quitting the application

Press the "Quit" menu or close the tab.

## Support

You can ask questions to mail arthur.macdonald@etu.u-bordeaux.fr

## Contributing

We are not opened to contributions.

## Authors and acknowledgment

Thanks to all people from Stack Overflow who created and answered post that helped us, we love y'all <3

## License

[![License: CC BY-NC 4.0](https://licensebuttons.net/l/by-nc/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc/4.0/)

## Project Status

Still in development
