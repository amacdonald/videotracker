import os, sys
import unittest
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from src.models.AllPoints import AllPoints
from src.models.Point import Point

class Test_AllPoints(unittest.TestCase):
    def setUp(self) -> None:
        self.AllPoints = AllPoints()
        self.X1 = 1
        self.Y1 = 8
        self.T1 = 0
        self.X2 = -2
        self.Y2 = 33
        self.T2 = 1
        self.X3 = 3
        self.Y3 = -4
        self.T3 = 2
        self.AllPoints.create_array(3)
    
    
    def test_savePoint(self):
        
        self.AllPoints.savePoint(self.X1, self.Y1, self.T1)
        self.AllPoints.savePoint(self.X2, self.Y2, self.T2)
        self.AllPoints.savePoint(self.X3, self.Y3, self.T3)
        
        self.assertTrue(type(self.AllPoints.allPointsList) is list)
        
        point_list = [Point(self.X1, self.Y1, self.T1), Point(self.X2, self.Y2, self.T2), Point(self.X3, self.Y3, self.T3)]
        equal = True
        for k in range(len(point_list)):
            equal = self.AllPoints.allPointsList[k].getX() is point_list[k].getX() and self.AllPoints.allPointsList[k].getY() is point_list[k].getY() and self.AllPoints.allPointsList[k].getTemps() is point_list[k].getTemps() and equal
            
        self.assertTrue(equal)
            
        not_point_list = [Point(self.X1, self.Y1, self.T1), Point(self.X1, self.Y1, self.T1), Point(self.X1, self.Y1, self.T1)]
        equal = True
        for k in range(len(not_point_list)):
            equal = self.AllPoints.allPointsList[k].getX() is not_point_list[k].getX() and self.AllPoints.allPointsList[k].getY() is not_point_list[k].getY() and self.AllPoints.allPointsList[k].getTemps() is point_list[k].getTemps() and equal
        
        self.assertFalse(equal)


    def test_deletePoint(self):
        point_list = [0, Point(self.X2,self.Y2, self.T2), Point(self.X3,self.Y3, self.T3)]
        self.AllPoints.allPointsList = [Point(self.X1,self.Y1, self.T1), Point(self.X2, self.Y2, self.T2), Point(self.X3, self.Y3, self.T3)]
        self.AllPoints.deletePoint(Point(self.X1, self.X2))
        
        equal = True
        for k in range(len(point_list)):
            if self.AllPoints.allPointsList[k] == 0:
                equal = self.AllPoints.allPointsList[k] is point_list[k] and equal
            else:
                equal = self.AllPoints.allPointsList[k].getX() is point_list[k].getX() and self.AllPoints.allPointsList[k].getY() is point_list[k].getY() and self.AllPoints.allPointsList[k].getTemps() is point_list[k].getTemps() and equal
            
        self.assertTrue(equal)
        
        not_point_list = [Point(self.X1,self.Y1), Point(self.X2,self.Y2), Point(self.X3,self.Y3)]
        equal = True
        for k in range(len(not_point_list)):
            if self.AllPoints.allPointsList[k] == 0:
                equal = self.AllPoints.allPointsList[k] is not_point_list[k] and equal
            else:
                equal = self.AllPoints.allPointsList[k].getX() is not_point_list[k].getX() and self.AllPoints.allPointsList[k].getY() is not_point_list[k].getY() and self.AllPoints.allPointsList[k].getTemps() is point_list[k].getTemps() and equal
        
        self.assertFalse(equal)
    
if __name__ == '__main__':
    unittest.main(verbosity=2)