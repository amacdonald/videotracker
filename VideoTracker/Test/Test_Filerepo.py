import os,sys
import csv
import unittest
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from src.models.Filerepo import *
from src.models.Point import Point


class Test_Filerepo(unittest.TestCase):
    
    def setUp(self) -> None:

        self.Point1 = Point(1, 2, 1)
        self.Point2 = Point(3, 4, 2)
        self.Point3 = Point(10, -3, 3)
        self.datas = [self.Point1, self.Point2, self.Point3]
        self.SaveFile = Filerepo()
        
        
    def test_export2CSV(self):
        #Export datas into a csv
        self.SaveFile.export2CSV(self.datas, "file.csv")

        #Read the new csv and save the read datas in readerOutput
        f = open("file.csv", "rt")
        reader = csv.reader(f, delimiter = ";")
        readerOutput = []
        for line in reader:
            readerOutput.append(line)
        f.close()

        #Compare
        self.assertEqual(readerOutput, [['1', '2', '1'],['3', '4', '2'],['10', '-3', '3']])
        self.assertNotEqual(readerOutput, [['0;2;1'],['3;4;2'],['10;-3;3']])

        #Delete the created file
        os.remove("file.csv") 
        

    def transformData2CSV(self):
        transformedData = self.SaveFile.transformData2CSV(self.datas)
        self.assertEqual(transformedData, '1;2\n3;4\n10;-3\n')
        self.assertNotEqual(transformedData, '8;2\n3;4\n10;-3\n')

if __name__ == '__main__':
    unittest.main(verbosity=2)