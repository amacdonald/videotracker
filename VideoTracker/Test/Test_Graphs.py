import os, sys
import unittest
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from src.models.Graphs import *

class Test_Graphs(unittest.TestCase):
    def setUp(self) -> None:
        self.graph = Graphs()
        self.pathname = currentdir + "/csv_test_file.csv"
    
    def test_create_Datas_X_Y(self):
        auto_df = self.graph.create_Datas_X_Y(self.pathname)
        good_manual_df = DataFrame({'X': [3,9,1,-2],'Y': [4,-1,7,9]} ,columns=['X','Y'])
        not_good_manual_df = DataFrame({'X': [3,9,2,-2],'Y': [4,-1,7,9]} ,columns=['X','Y'])
        self.assertTrue(auto_df.equals(good_manual_df))
        self.assertFalse(auto_df.equals(not_good_manual_df))

if __name__ == '__main__':
    unittest.main(verbosity=2)