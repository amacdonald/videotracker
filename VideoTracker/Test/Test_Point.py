import os, sys
import unittest
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from src.models.Point import * 

class Test_Point(unittest.TestCase):
    def setUp(self) -> None:
        self.Point = Point()
        self.X1 = 3
        self.Y1 = 9
        self.X2 = -6
        self.Y2 = 13

    def test_setXSmallInt(self):
        self.Point.set_X(self.X1)
        self.assertTrue(self.Point.getX() is self.X1)
        self.assertFalse(self.Point.getX() is self.Y1)
    
    def test_setYSmallInt(self):
        self.Point.set_Y(self.Y1)
        self.assertTrue(self.Point.getY() is self.Y1)
        self.assertFalse(self.Point.getY() is self.X1)

    def test_XNegative(self):
        self.Point.set_X(self.X2)
        self.assertTrue(self.Point.getX() is self.X2)
        self.assertFalse(self.Point.getX() is self.X1)
    
    def test_YTwoNumberInt(self):
        self.Point.set_Y(self.Y2)   
        self.assertTrue(self.Point.getY() is self.Y2)
        self.assertFalse(self.Point.getY() is self.X1)

if __name__ == '__main__':
    unittest.main(verbosity=2)
