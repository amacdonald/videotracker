import os, sys
import unittest
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import tkinter as tk
import cv2

from src.models.Video import Video

class Test_Video(unittest.TestCase):
    def setUp(self) -> None:
        self.window = tk.Tk()
        self.video = Video(self.window)
        self.test_video_path = currentdir + "/video_test.mp4"
        self.video.openFile(self.test_video_path)

    def test_get_frame(self):
        image = self.video.get_frame()
        imageBis = cv2.VideoCapture(self.test_video_path).read()
        print(image == imageBis )
        
        
        self.video.quitVideo()
        self.video.quitApp(self.window)
        

    
if __name__ == '__main__':
    unittest.main(verbosity=2)