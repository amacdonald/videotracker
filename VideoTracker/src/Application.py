from re import A
import tkinter as tk
import sys
from models.Video import Video
from views.ViewMain import ViewMain
from views.ViewTable import ViewTable
from models.Point import Point
from models.Filerepo import Filerepo
from models.AllPoints import AllPoints
from models.Graphs import Graphs
from models.Map import Map
from controllers.Controller import Controller


class Application(tk.Tk):

    def __init__(self):

        super().__init__()
        self.title('Video Tracker')
        # create a main view and place it on the root window
        viewMain = ViewMain(self)
        # initialize Frame "viewTable"
        viewTableClass = ViewTable
        # create a video model
        video = Video(self)
        # create a filerepo
        filerepo = Filerepo()
        #create allPoints
        allPoints = AllPoints()
        # create a graph
        graphs = Graphs()
        #create the map
        map = Map()
        # create a controller
        controller = Controller(video, viewMain, viewTableClass, filerepo, allPoints, graphs, map)

        # set the controller to the main view
        viewMain.setController(controller)
        
        viewMain.init_window_interface()
        

if __name__ == '__main__':
    app = Application()
    app.mainloop()