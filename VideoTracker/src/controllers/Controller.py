from tkinter import messagebox

class Controller:

    def __init__(self, video, viewMain, viewTableClass, filerepo, allPoints, graphs, map):
        
        self.video = video
        self.viewMain = viewMain
        self.viewTableClass = viewTableClass
        self.filerepo = filerepo
        self.allPoints = allPoints
        self.graphs = graphs
        self.map = map
        
    def set_time(self, time):
        self.allPoints.create_array(time)
        
    def openVideo(self):
        self.video.openFile(self.change_frame)
        self.set_time(self.video.number_of_frame + 1)
        
    def right_clicked(self, event):
        self.map.setup_Scale([event.x, event.y])
        if self.map.set_scale_progress == 1:
            self.viewMain.show_validation_message("First point successufy placed")
        if self.map.set_scale_progress == 2:
            self.show_line(self.map.scale_position_1, self.map.scale_position_2)
        
    def reset_scale(self):
        try:
            self.map.reset_Scale(self.viewMain.scale_line, self.video.canvas, self.viewMain.scale_legend)
        except:
            messagebox.showerror(title="ERROR", message="No scale created")
        
    def show_line(self, position1:list, position2:list):
        self.viewMain.create_a_scale(self.video.canvas, position1, position2, self.map.scale_IRL)
    
    def set_origin_active(self):
        if self.video.open:
            self.video.canvas.bind("<Button-1>", self.set_origin)
        else:
            messagebox.showerror(title="ERROR", message="No video is opened")
        
    def set_origin(self,event):
        try:
            self.video.canvas.delete(self.viewMain.origin)
        except:
            pass
        self.map.setup_Origin([event.x, event.y])
        self.viewMain.show_origin(self.video.canvas, self.map.origin_position[0], self.map.origin_position[1])
        self.video.canvas.unbind("<Button-1>")
        self.viewMain.show_validation_message("Origin has been placed")
    
    def set_points_active(self):
        if self.map.origin_position != [0, 0]:
            self.video.canvas.bind("<Button-1>", self.set_points)
            self.viewMain.parent.bind("<Escape>", lambda x: self.set_points_desactivate())
            self.map.showPoints = True
            self.change_frame()
        else:
            messagebox.showerror(title="ERROR", message="No origin has been placed")
    
    def set_points_desactivate(self):
        self.map.showPoints = False
        self.video.canvas.unbind("<Button-1>")
        self.video.canvas.unbind("<Escape>")
        self.viewMain.delete_origin_and_point(self.video.canvas)
    
    def set_points(self, event):
        point = [event.x, event.y]
        try:
            self.video.canvas.delete(self.viewMain.current_point)
        except:
            pass
        map_related_point = self.map.transform_canvas_pos_to_map_pos(point)
        self.allPoints.saveMapPoint(map_related_point[0], map_related_point[1], self.video.current_frame)
        self.allPoints.savePoint(point[0], point[1], self.video.current_frame)
        
        self.forward_frame()
        
    def change_frame(self):
        try:
            self.viewMain.delete_origin_and_point(self.video.canvas)
        except:
            pass
        if self.map.showPoints:
            try:
                self.viewMain.show_origin(self.video.canvas, self.map.origin_position[0], self.map.origin_position[1])
                try:
                    current_point = self.allPoints.allPointsList[self.video.current_frame]
                    self.viewMain.show_current_point(self.video.canvas, current_point.getX(), current_point.getY())
                except:
                    pass
                try:
                    self.show_line(self.map.scale_position_1, self.map.scale_position_2)
                except:
                    pass
            except:
                pass
    
    def start_video(self, btn):
        self.video.pausing(btn, self.change_frame)
    
    def forward_frame(self):
        self.video.nextFrame()
        self.change_frame()
        
    def backward_frame(self):
        self.video.previousFrame()
        self.change_frame()
            
    def fast_forward_or_backward(self, choice:int):
        self.video.fastforward_fastbackward(choice)
        self.change_frame()
        
    def transform_and_save_Points(self, auto:bool=False):
        try:
            self.allPoints.transform_canvas_values_to_map_scale_values(self.allPoints.getPoints(), self.map.scale_values)
            if auto:
                try:
                    self.filerepo.export2CSV(self.allPoints.allSavablePoints, self.filerepo.saved_file_path)
                except:
                    self.filerepo.export2CSV(self.allPoints.allSavablePoints, self.filerepo.selectExportFile())
            else:        
                self.filerepo.export2CSV(self.allPoints.allSavablePoints, self.filerepo.selectExportFile())
        except:
            messagebox.showerror(title="ERROR", message="Error while saving, did you opened a video or selected a valid save file ?")
            
    def show_value_table(self):
        self.viewTable = self.viewTableClass(self.viewMain.parent)
        self.viewTable.setController(self)
        self.viewTable.launch_table_window()