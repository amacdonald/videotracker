import sys
try:
    from models.Point import Point
except:
    from src.models.Point import Point


class AllPoints:
    
    def create_array(self, time:int):
        """Create an array of *time* element

        Args:
            time (int): number of frame of the video
        """
        self.allPointsList = [0 for k in range(time + 1)]
        self.all_map_related_point = [0 for k in range(time + 1)]
        self.allSavablePoints = [0 for k in range(time + 1)]
       
        
    def getPoints(self):
        return self.all_map_related_point
    
    
    def savePoint(self, pointX:float, pointY:float, pointTemps:int):
        """Insert a point at the position pointTemps

        Args:
            pointX (float): Point position X
            pointY (float): Point position Y
            pointTemps (int): Point position Time
        """
        self.allPointsList[pointTemps] = Point(pointX, pointY, pointTemps)
        
    def saveMapPoint(self, pointX:float, pointY:float, pointTemps:int):
        self.all_map_related_point[pointTemps] = Point(pointX, pointY, pointTemps)
    
    def deletePoint(self, point:Point):
        """Change the value of the *Point Time* element to the default value (0)

        Args:
            point (Point): an object Point()
        """
        self.allPointsList[point.getTemps()] = 0
        self.all_map_related_point[point.getTemps()] = 0


    def transform_canvas_values_to_map_scale_values(self, values:list, scale_values:list):
        """_
        Args:
            values (list): list of Points
            scale_values (list): list of the scale lenght and mm
        """
        for i in range(len(values)):
            if values[i] != 0:
                new_X = (values[i].getX()/scale_values[0])*scale_values[1]
                new_Y = (values[i].getY()/scale_values[0])*scale_values[1]
                self.save_point_to_savablelist(int(new_X), int(new_Y), values[i].getTemps())
                
    def save_point_to_savablelist(self, pointX:float, pointY:float, pointTemps:int):
        self.allSavablePoints[pointTemps] = Point(pointX, pointY, pointTemps)