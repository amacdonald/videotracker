from tkinter import filedialog
import os


class Filerepo:
    
    def selectExportFile(self):
        '''
        Choose the file to save (.csv)
        file: string .csv file
        '''
        file = filedialog.asksaveasfilename(initialdir= os.getcwd() + "/VideoTracker/ressources/csvFiles", initialfile="Points", defaultextension=".csv", filetypes=[("csv Files", '.csv')])
        
        if file != ():
            assert file[-4:] == ".csv", 'Unvalid file name, must be .csv file'
            
            self.saved_file_path = file
            
            return file
        return "No file"


    def export2CSV(self, data:list, pathname:str) -> None:
        '''
        Save data to a csv file
        data: list of element Point
        auto: bool to auto save or not
        '''
        
        dataStr = self.transformData2CSV(data)
        
        try:
            f = open(pathname, 'w')
            
            f.write(dataStr)

            f.close()
        except:
            print("Couldn't open file")


    def transformData2CSV(self, data:list, separator = ";") -> str:

        '''
        Transform data into writable format
        data: list of element 'Point'
        '''

        dataStr = ""
        for element in data:
            #None value is 0, skipping operation when value = 0
            if element != 0:
                dataStr += str(element.getX()) + separator + str(element.getY()) + separator + str(element.getTemps()) + "\n"
        return dataStr