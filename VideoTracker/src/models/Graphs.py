import csv
from tkinter.filedialog import askopenfilename
import os
from pandas import DataFrame

class Graphs:
    def create_Datas_X_Y(self, path, separator = ";"):
        X = []
        Y = []
        
        with open(path,'r') as csvfile:
            lines = csv.reader(csvfile, delimiter=separator)
            for row in lines:
                X.append(int(row[0]))
                Y.append(int(row[1]))
        
        datas = {'X': X,'Y': Y}  
        
        df = DataFrame(datas,columns=['X','Y'])
        
        return df
    
    def create_Datas_X_T(self, path, separator = ";"):
        X = []
        T = []
        
        with open(path,'r') as csvfile:
            lines = csv.reader(csvfile, delimiter=separator)
            for row in lines:
                X.append(int(row[0]))
                T.append(int(row[2]))
        
        datas = {'X': X,'T': T}  
        
        df = DataFrame(datas,columns=['X','T'])
        
        return df
    
    def create_Datas_Y_T(self, path, separator = ";"):
        Y = []
        T = []
        
        with open(path,'r') as csvfile:
            lines = csv.reader(csvfile, delimiter=separator)
            for row in lines:
                Y.append(int(row[1]))
                T.append(int(row[2]))
        
        datas = {'Y': Y,'T': T}  
        
        df = DataFrame(datas,columns=['Y','T'])
        
        return df
    
    def select_Path(self):
        file = askopenfilename(initialdir= os.getcwd() + "/VideoTracker/ressources/csvFiles", filetypes=[('CSV files', '*.csv')])
        
        if file != ():
            assert file[-4:] == ".csv", 'Unvalid file name, must be .csv file'
            return file
        
        return "No file"