import tkinter as tk
from tkinter import messagebox

class Map:
    
    def __init__(self):
        self.set_scale_progress = 0
        self.scale_position_1 = [0, 0]
        self.scale_position_2 = [0, 0]
        self.scale_IRL = 0
        self.origin_position = [0, 0]
        self.scale_values = [1, 1]
        
        self.showPoints = False
    
    def setup_Scale(self, position:list, custom_scale_value:int=0):
        if self.set_scale_progress == 0:
            self.scale_position_1 = position
            self.set_scale_progress += 1
            
        elif self.set_scale_progress == 1:
            self.scale_position_2 = position
            self.set_scale_progress += 1
            try:
                self.scale_IRL = tk.simpledialog.askstring('Scale', 'What is this scale equal to in real life ? (in millimeters)')
            except:
                self.scale_IRL = custom_scale_value
            if self.scale_IRL.isnumeric():
                #set the scale horizontaly or verticaly
                if max(self.scale_position_1[0], self.scale_position_2[0]) - min(self.scale_position_1[0], self.scale_position_2[0]) < max(self.scale_position_1[1], self.scale_position_2[1]) - min(self.scale_position_1[1], self.scale_position_2[1]):
                    self.scale_position_2[0] = self.scale_position_1[0]
                else:
                    self.scale_position_2[1] = self.scale_position_1[1]
                #scale_values is height and lenght by cm
                self.scale_values = [max(abs(self.scale_position_2[0] - self.scale_position_1[0]), abs(self.scale_position_2[1] - self.scale_position_1[1])), int(self.scale_IRL)]
            else:
                self.reset_Scale()
                messagebox.showwarning(title='Warning', message='Wrong scale value (must be a round number)')
    
    def reset_Scale(self, line=None, canvas=None, legend=None):
        canvas.delete(line)
        legend.destroy()
        self.set_scale_progress = 0
        self.scale_position_1 = [0, 0]
        self.scale_position_2 = [0, 0]
        self.scale_IRL = 0
        messagebox.showinfo(title="Success", message="Scale successfully reset")
        
    def setup_Origin(self, position:list):
        self.origin_position = position
    
    def transform_canvas_pos_to_map_pos(self, position:list):
        map_position = [position[0] - self.origin_position[0], self.origin_position[1] - position[1]]
        return map_position
    
    def transform_map_pos_to_canvas_pos(self, position:list):
        canvas_position = [position[0] + self.origin_position[0],-position[1] + self.origin_position[1]]
        return canvas_position