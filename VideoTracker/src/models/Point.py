class Point:
    
    def __init__(self, x = 0, y = 0, temps = 0):

        '''
        Set a point with 2 coordinates x y
        x: float
        y: float
        temps: int (time in frame)
        '''

        self.x = x
        self.y = y
        self.temps = temps


    def __str__(self):
        
        result = str(self.x) + ',' + str(self.y) + ',' + str(self.temps)
        return result


    def getX(self) -> float:
        return self.x
    
    
    def getY(self) -> float:
        return self.y


    def set_X(self, x:float) -> None:
        self.x = x


    def set_Y(self, y:float) -> None:
        self.y = y
        
    def getTemps(self):
        return self.temps