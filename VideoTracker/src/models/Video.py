import cv2
from tkinter import messagebox
from tkinter import *
import PIL.Image, PIL.ImageTk
from tkinter.filedialog import askdirectory, askopenfilename
import os
from tkinter import ttk

class Video:
    
    def __init__(self, window):
        self.delay = 30
        self.window = window
        
        self.canvas = Canvas(self.window, bg="#38383f", highlightthickness=0)
        self.canvas.pack(fill = BOTH, expand=1)

        self.open = False
        self.anti_resize_lag = 0
    
    def resize_video(self):
        self.anti_resize_lag += 1
        
        #Change the value "10" if your PC slows down when resizing window. May add some small resizing issues
        if self.anti_resize_lag > 15 or not self.open:
            self.anti_resize_lag = 0
            try:
                self.canvas.pack(fill = BOTH, expand=1)
                self.canvas.update_idletasks() 
                self.canvas_width = self.canvas.winfo_width()
                self.canvas_height = self.canvas.winfo_height()
                self.ratio = min((self.canvas_width - 50)/self.width, (self.canvas_height - 50)/self.height)
                self.delay = int(self.cap.get(cv2.CAP_PROP_FPS)/self.ratio)
                if self.pause:
                    resized_image = self.current_image.resize((int(self.width*self.ratio), int(self.height*self.ratio)))
                
                    self.photo = PIL.ImageTk.PhotoImage(image = resized_image)
                    self.canvas.create_image(self.canvas_width//2, (self.canvas_height-20)//2, image = self.photo, anchor= CENTER)
            except:
                pass
        
    def openFile(self, on_frame_change, file = ""):
        if not self.open:
            if file != "":
                filename = file
            else:
                filename = askopenfilename(initialdir= os.getcwd() + "/VideoTracker/ressources/videos", filetypes=[('Video files (mp4 or avi)', '*.mp4 *.avi')])
            
            if filename == () or filename == "":
                pass
            
            elif filename[-4:] not in [".mp4", ".avi"] :
                messagebox.showerror(title='Alert', message='Not .mp4 file')
                
            else: 
                self.pause = True
                self.ended = False
                self.cap = cv2.VideoCapture(filename)
                
                self.number_of_frame = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
                
                self.width = self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)
                self.height = self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
                
                self.resize_video()
                self.open = True
                
                self.create_timeline(on_frame_change)
                
                self.current_frame = -1
                
                #read first frame
                self.play_video()
            
        else:
            messagebox.showwarning(title='Warning', message='Video is already opened')
        
        
    def get_frame(self):
        try:
            if self.cap.isOpened() and not self.ended:
                self.current_frame += 1
                ret, frame = self.cap.read()
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        except:
            self.ended = True
            self.pausing(self.pauseButton)
            messagebox.showwarning(title='Alert', message='End of the video.')
        
        
    def play_video(self, on_frame_change=None):
        try:
            ret, frame = self.get_frame()
            if ret:
                img = PIL.Image.fromarray(frame)
                self.current_image = img
                
                resized_image = img.resize((int(self.width*self.ratio), int(self.height*self.ratio)))
                
                self.photo = PIL.ImageTk.PhotoImage(image = resized_image)
                self.canvas.create_image(self.canvas_width//2, (self.canvas_height-15)//2, image = self.photo, anchor= CENTER)
                
                self.timeline_follow()
                
                if on_frame_change != None:
                    on_frame_change()
            
            if not self.pause:
                self.window.after(self.delay, lambda: self.play_video(on_frame_change))
                
        except:
            messagebox.showwarning(title='Alert', message='End of the video.')
    
    def start_video(self, btn ,on_frame_change):
        self.pausing(btn, on_frame_change)
        return
            
    def readVideo(self, btn):
        if self.open:
            if self.pause:
                self.pausing(btn)
        else:
            messagebox.showwarning(title='Warning', message='No video is opened')
        
        
    def nextFrame(self):
        if self.open:
            if self.pause:
                self.play_video()
        else:
            messagebox.showwarning(title='Warning', message='No video is opened')
                
                
    def previousFrame(self):
        self.ended = False
        if self.open:
            if self.pause:
                if self.current_frame > 0:
                    self.current_frame -= 2
                    self.cap.set(cv2.CAP_PROP_POS_FRAMES, self.current_frame)
                    self.play_video()
                else:
                    messagebox.showerror(title='Alert', message='Beginning of the video')
        else:
            messagebox.showwarning(title='Warning', message='No video is opened')


    def quitVideo(self):
        if self.open:
            if self.pause:
                self.cap.release()
                self.open = False
                self.canvas.delete('all')
                self.timeline.pack_forget()
            else:
                self.pausing(self.pauseButton)
                messagebox.showwarning(title='Warning', message='Pause the video or wait for the end before closing it please.')
        else:
            messagebox.showwarning(title='Warning', message='No video is opened')
            
            
    def pausing(self, btn, on_frame_change):
        self.pauseButton = btn
        try:
            if self.open:
                if self.pause and not self.ended:
                    btn.config(text="PAUSE")
                    #To know if the button is PAUSE (for timeline)
                    self.btn_state = "PAUSE"
                    self.pause = False
                    self.play_video(on_frame_change)
                else:
                    btn.config(text="PLAY")
                    #To know if the button is PLAY (for timeline)
                    self.btn_state = "PLAY"
                    self.pause = True
            elif self.ended:
                messagebox.showerror(title='Alert', message='End of the video')
            else:
                messagebox.showwarning(title='Warning', message='No video is opened')
        except:
            messagebox.showwarning(title='Warning', message='No video is opened')
    
    
    def quitApp(self, window):
        self.__del__()
        window.destroy()
        

    def fastforward_fastbackward(self, back_or_forw):
        '''back_or_forw must be 0(backward) or 1(forward)'''
        if self.open:
            self.ended = False
            self.current_frame = int(back_or_forw*self.number_of_frame-1)
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, self.current_frame)
            if self.pause:
                self.play_video()
        else:
            messagebox.showwarning(title='Warning', message='No video is opened')
        
        
        
    #TimeLine
    def set_to_frame_timeline(self, current_frame, on_frame_change=None):
        if not self.follow:
            self.ended = False
            self.current_frame = current_frame
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, current_frame)
            self.play_video(on_frame_change)
        
        
    def timeline_follow(self):
        self.timeline.set(self.current_frame)
        self.follow = True
    
    def set_follow_false(self):
        self.follow = False
        
    def change_pause_state_True(self):
        self.pause = True
    
    def change_pause_state_False(self):
        try:
            if self.btn_state == "PAUSE":
                self.pause = False
                self.play_video()
        except:
            pass

    
    def create_timeline(self, on_frame_change):
        #Not good but couldn't find a way to insert it into view (setting must pass by controller to set the timeline)
        self.timeline = Scale(self.canvas, orient='horizontal', width=15, from_=0, to=self.number_of_frame-1, showvalue=0, command = lambda x: self.set_to_frame_timeline(self.timeline.get(), on_frame_change))
        self.timeline.config(troughcolor = '#252525', bd = 0, sliderlength = 20, highlightbackground = "#252525", bg = 'white', activebackground = "#353535", relief = FLAT)
        self.timeline.pack(side = BOTTOM, fill = X)
        self.timeline.bind("<B1-Motion>", lambda x:self.set_follow_false())
        self.timeline.bind("<ButtonPress-1>", lambda x:self.change_pause_state_True())
        self.timeline.bind("<ButtonRelease-1>", lambda x:self.change_pause_state_False())
        
        
    def __del__(self):
        try:
            if self.cap.isOpened():
                self.cap.release()
        except:
            pass

