from cProfile import label
from re import L
import tkinter as tk
import PIL.Image, PIL.ImageTk
from tkinter import *
from tkinter import messagebox
import platform
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class ViewMain(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        parent.configure(bg="#26142b")

        if platform.system() == "Linux":
            parent.attributes("-zoomed", True)
        elif platform.system() == "Windows":
            parent.state("zoomed")

        screen_width = parent.winfo_screenwidth()
        screen_height = parent.winfo_screenheight()
        parent.minsize(screen_width // 2, screen_height // 2)

        self.menu = Menu(parent, tearoff=0, bg="#1d1a21", bd=0, activeborderwidth=0)

        parent["menu"] = self.menu

        self.parent = parent

    def init_window_interface(self):

        self.parent.bind("<Configure>", lambda x: self.controller.video.resize_video())

        navigationButtons = Frame(self.parent, bg="#1d1a21")
        navigationButtons.pack(side=BOTTOM, fill=X)

        buttonPause = Button(
            navigationButtons,
            text="PLAY",
            width=10,
            pady=10,
            relief=FLAT,
            highlightthickness=0,
            bg="#0d1017",
            fg="white",
            activebackground="#584862",
            activeforeground="white",
        )
        buttonPause.config(command=lambda: self.controller.start_video(buttonPause))
        buttonPause.pack(side=BOTTOM, pady=10)

        menuFile = Menu(self.menu, tearoff=0, bg="#1d1a21", bd=0, activeborderwidth=0)

        menuFile.add_command(
            label="Open Video",
            underline=0,
            accelerator="CTRL+O",
            command=lambda: self.controller.openVideo(),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        self.bind_all("<Control-o>", lambda x: self.controller.openVideo())

        menuFile.add_command(
            label="Read Video",
            underline=0,
            accelerator="CTRL+R",
            command=lambda: self.controller.video.readVideo(buttonPause),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        self.bind_all(
            "<Control-r>", lambda x: self.controller.video.readVideo(buttonPause)
        )

        menuFile.add_command(
            label="Quit Video",
            underline=0,
            accelerator="CTRL+Q",
            command=lambda: self.controller.video.quitVideo(),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        self.bind_all("<Control-q>", lambda x: self.controller.video.quitVideo())

        menuFile.add_command(
            label="Save Points",
            underline=0,
            accelerator="CTRL+S",
            command=lambda: self.controller.transform_and_save_Points(True),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        self.bind_all(
            "<Control-s>",
            lambda x: self.controller.transform_and_save_Points(True)
        )

        menuFile.add_command(
            label="Save as",
            accelerator="CTRL+ALT+S",
            command=lambda: self.controller.transform_and_save_Points(),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        self.bind_all(
            "<Control-Alt-q>",
            lambda x: self.controller.transform_and_save_Points()
        )

        menuFile.add_separator()
        menuFile.add_command(
            label="Quit",
            accelerator="CTRL+ALT+Q",
            command=lambda: self.controller.video.quitApp(self.parent),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        
        self.bind_all(
            "<Control-Alt-q>", lambda x: self.controller.video.quitApp(self.parent)
        )

        menuView = Menu(self.menu, tearoff=0, bg="#1d1a21", bd=0, activeborderwidth=0)
        menuGraphs = Menu(menuView, tearoff=0, bg="#1d1a21", activeborderwidth=0)
        
        menuGraphs.add_command(
            label="y(x)",
            command=lambda: self.show_Graph('Y', 'X'),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        menuGraphs.add_command(
            label="x(t)",
            command=lambda: self.show_Graph('X', 'T'),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        menuGraphs.add_command(
            label="y(t)",
            command=lambda: self.show_Graph('Y', 'T'),
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        menuView.add_cascade(
            label="Graphs",
            menu=menuGraphs,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        
        menuEdit = Menu(self.menu, tearoff=0, bg="#1d1a21", activeborderwidth=0)
        
        menuEdit.add_command(
            label="Reset Scale",
            command= self.controller.reset_scale,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        menuEdit.add_command(
            label="Set Origin",
            command= self.controller.set_origin_active,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        menuEdit.add_command(
            label="Add Points",
            command= self.controller.set_points_active,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        menuEdit.add_command(
            label="Show values",
            command= self.controller.show_value_table,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        
        
        self.menu.add_cascade(
            label="Files",
            accelerator="ALT+F",
            underline=0,
            menu=menuFile,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        self.menu.add_cascade(
            label="View",
            menu=menuView,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )
        self.menu.add_cascade(
            label="Edit",
            menu=menuEdit,
            foreground="white",
            activebackground="#464a6c",
            activeforeground="white",
        )


        Button(
            navigationButtons,
            text=">",
            command=lambda: self.controller.forward_frame(),
            pady=10,
            relief=FLAT,
            highlightthickness=0,
            bg="#0d1017",
            fg="white",
            activebackground="#584862",
            activeforeground="white",
        ).place(relx=0.60, rely=0.5, anchor=CENTER)
        Button(
            navigationButtons,
            text="<",
            command=lambda: self.controller.backward_frame(),
            pady=10,
            relief=FLAT,
            highlightthickness=0,
            bg="#0d1017",
            fg="white",
            activebackground="#584862",
            activeforeground="white",
        ).place(relx=0.40, rely=0.5, anchor=CENTER)

        Button(
            navigationButtons,
            text=">>",
            command=lambda: self.controller.fast_forward_or_backward(1),
            pady=10,
            relief=FLAT,
            highlightthickness=0,
            bg="#0d1017",
            fg="white",
            activebackground="#584862",
            activeforeground="white",
        ).place(relx=0.65, rely=0.5, anchor=CENTER)
        Button(
            navigationButtons,
            text="<<",
            command=lambda: self.controller.fast_forward_or_backward(0),
            pady=10,
            relief=FLAT,
            highlightthickness=0,
            bg="#0d1017",
            fg="white",
            activebackground="#584862",
            activeforeground="white",
        ).place(relx=0.35, rely=0.5, anchor=CENTER)

        self.controller.video.canvas.bind("<Button-3>", self.controller.right_clicked)
        
        
    def show_Graph(self, first_letter, second_letter):
        """Show the graph of first_letter(second_letter)

        Args:
            first_letter (str): First letter must be X or Y
            second_letter (str): second letter must me Y or T
        """
        if first_letter == 'Y':
            if second_letter == 'X':
                df = self.controller.graphs.create_Datas_X_Y(self.controller.graphs.select_Path())
            else:
                df = self.controller.graphs.create_Datas_Y_T(self.controller.graphs.select_Path())
        else:
            df = self.controller.graphs.create_Datas_X_T(self.controller.graphs.select_Path())
            
        graphWindow = Toplevel(self.parent)
        
        graphWindow.title(first_letter + "(" + second_letter + ") points positions")
        
        graphWindow.geometry("1220x620")
        graphWindow.grab_set()

        figure = plt.figure(figsize=(12,6))
        plt.scatter(df[second_letter], df[first_letter], c='yellow')
        if second_letter != 'T':
            plt.plot([0,0], color='red')
        plt.legend(["Points"])
        plt.xlabel(second_letter)
        plt.ylabel(first_letter, rotation=0)
        scatter = FigureCanvasTkAgg(figure, graphWindow)
        scatter.get_tk_widget().place(x=10, y=10)
        
        plt.title(first_letter + "(" + second_letter + ") points positions")

    def show_validation_message(self, message_text:str):
        self.temporary_message = Label(self.controller.video.canvas, text=message_text, bg= "#38383f", fg = "white")
        self.temporary_message.place(x=10, y=10)
        self.temporary_message.after(1200, lambda: self.temporary_message.destroy())
        
    def create_a_scale(self, canvas, position1:list, position2:list, scale_value_IRL:str):
        self.scale_line = canvas.create_line(position1[0] , position1[1], position2[0], position2[1], width = 5, fill = "white")
        self.scale_legend = Label(self.controller.video.canvas, text=scale_value_IRL + " mm", bg= 'white')
        self.scale_legend.place(x=(position1[0] + position2[0])//2, y=(position1[1] + position2[1])//2)
    
    def show_origin(self, canvas, pos1, pos2):
        self.origin = canvas.create_oval(pos1 + 7 ,pos2 + 7 ,pos1 - 9 ,pos2 - 9 , fill = "red")
        
    def show_current_point(self, canvas, pos1, pos2):
        self.current_point = canvas.create_oval(pos1 + 5 ,pos2 + 5 ,pos1 - 7 ,pos2 - 7 , fill = "yellow")
    
    def delete_origin_and_point(self, canvas):
        canvas.delete(self.origin)
        canvas.delete(self.current_point)
        
    def setController(self, controller):
        self.controller = controller