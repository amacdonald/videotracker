from re import L
import tkinter as tk
from tkinter import *
from tkinter import messagebox
from  tkinter import ttk

class ViewTable(tk.Toplevel):
    
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        
        
    def launch_table_window(self):
        try:
            self.show_window()
            
            self.create_table_frame()
            self.create_scrollbar()
            self.setup_columns()
            self.values_table.pack(padx=20)
            
            self.add_datas(self.controller.allPoints.all_map_related_point)
            
            self.setup_labels()
            self.setup_entry_boxes()
            self.setup_buttons()
        except:
            self.destroy()
            self.update()
            tk.messagebox.showerror(title="ERROR", message="No point placed")
        
    def show_window(self):
        self.title("Points Table")
        
        self.grab_set()
    
    def create_table_frame(self):
        self.table = Frame(self)
        self.table.pack()
        
    def create_scrollbar(self):
        self.scroll_bar = ttk.Scrollbar(self, orient="vertical")
        self.scroll_bar.pack(side=RIGHT, fill=Y)
        
        self.values_table = ttk.Treeview(self, yscrollcommand=self.scroll_bar.set)
        
        self.scroll_bar.config(command=self.values_table.yview)
        
    def setup_columns(self):
        self.values_table['columns'] = ('X', 'Y', 'T')
        
        self.values_table.column("#0", width=0,  stretch=NO)
        self.values_table.column("X",anchor=CENTER, width=40)
        self.values_table.column("Y",anchor=CENTER,width=40)
        self.values_table.column("T",anchor=CENTER,width=60)
        
        self.values_table.heading("#0", text="", anchor=CENTER)
        self.values_table.heading("X", text="X", anchor=CENTER)
        self.values_table.heading("Y", text="Y", anchor=CENTER)
        self.values_table.heading("T", text="Frame", anchor=CENTER)
        
    def add_datas(self, datas:list):
        """Add datas to the table

        Args:
            datas (list): must me a List of element Point or 0 (from AllPoint)
        """
        for i in range(len(datas)):
            if datas[i] != 0:
                self.values_table.insert(parent='',index='end',iid=i,text='',values=(str(datas[i].getX()),str(datas[i].getY()),str(datas[i].getTemps())))
        
    def setup_labels(self):
        self.edit_zone = Frame(self)
        self.edit_zone.pack(pady=10, padx=5)
        
        self.Xvalue= Label(self.edit_zone,text = "X")
        self.Xvalue.grid(row=0,column=0 )

        self.Yvalue = Label(self.edit_zone, text = "Y")
        self.Yvalue.grid(row=0,column=1)
        
    def setup_entry_boxes(self):
        self.Xvalue_entry = Entry(self.edit_zone)
        self.Xvalue_entry.grid(row= 1, column=0)

        self.Yvalue_entry = Entry(self.edit_zone)
        self.Yvalue_entry.grid(row=1,column=1)
    
    def select_values(self):
        self.Xvalue_entry.delete(0,END)
        self.Yvalue_entry.delete(0,END)
        
        self.selected_row = self.values_table.focus()
        self.selected_values = self.values_table.item(self.selected_row,'values')

        self.Xvalue_entry.insert(0,self.selected_values[0])
        self.Yvalue_entry.insert(0,self.selected_values[1])
        
    def save_value(self):
        self.selected_row = self.values_table.focus()
        New_Values = [int(self.Xvalue_entry.get()), int(self.Yvalue_entry.get()), int(self.selected_values[2])]
        self.values_table.item(self.selected_row,text="",values=(New_Values[0], New_Values[1], New_Values[2]))
        
        self.controller.allPoints.saveMapPoint(New_Values[0], New_Values[1], New_Values[2])
        
        canvas_pos = self.controller.map.transform_map_pos_to_canvas_pos(New_Values[:-1])
        self.controller.allPoints.savePoint(canvas_pos[0], canvas_pos[1], New_Values[2])
        
        self.controller.change_frame()
    
    def setup_buttons(self):
        self.select_button = Button(self,text="Select values", command=self.select_values)
        self.select_button.pack(pady =10)
        
        self.save_button = Button(self,text="Save ",command=self.save_value)
        self.save_button.pack(pady = 10)
        
    def setController(self, controller):
        self.controller = controller